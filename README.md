# **Box Office Performance Report** 

I leveraged 2012-2016 box office data to guide Netflix's strategic acquisition of older movies for their platform.

## Dataset:
- 2012-2016 Box Office Movie Data
